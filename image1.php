<?php 

header("Content-type: image/png");

$width = 300;
$height = 300;

$image = imagecreate($width, $height);

imagecolorallocate($image, $_GET['r'], $_GET['g'], $_GET['b']);

imagepng($image);

imagedestroy($image);
