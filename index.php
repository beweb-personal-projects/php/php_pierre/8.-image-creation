<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <form class="col-6 p-5" method="GET" action="http://localhost:1666/">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">RED: (R)</label>
                    <input type="number" min="0" max="255" class="form-control" name="r">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">GREEN: (G)</label>
                    <input type="number" min="0" max="255" class="form-control" name="g">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">BLUE: (B)</label>
                    <input type="number" min="0" max="255" class="form-control" name="b">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <div class="col-6 p-5">
                <?php 

                    if(isset($_GET['r']) && isset($_GET['g']) && isset($_GET['b'])) {
                        if($_GET['r'] === "") $_GET['r'] = 0;
                        if($_GET['g'] === "") $_GET['g'] = 0;
                        if($_GET['b'] === "") $_GET['b'] = 0;

                        echo "<img src='image1.php/?r={$_GET['r']}&g={$_GET['g']}&b={$_GET['b']}'>"; 
                    } else {
                        echo "<img src='image1.php/?r=100&g=100&b=100'>"; 
                    }
                    
                ?>
            </div>
        </div>
    </div>
</body>
</html>